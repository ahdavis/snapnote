/*
 * ListStack.ts
 * Defines a linked-list implementation of the Stack ADT
 * Created on 9/15/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { Stack } from './Stack';

/**
 * A linked-list implementation of a [[Stack]]
 */
export class ListStack<T> implements Stack<T> {
	//fields
	/**
	 * The size of the stack
	 */
	private _size: number;

	/**
	 * The top of the stack
	 */
	private _head: ListNode<T> | null;

	//methods
	
	/**
	 * Constructs a new `ListStack` instance
	 */
	constructor() {
		this._size = 0; //a new stack has no elements
		this._head = null; //ditto
	}

	/**
	 * Gets the size of the `ListStack`
	 *
	 * @returns The number of elements in the stack
	 */
	public get size(): number {
		return this._size;
	}

	/**
	 * Pushes a value onto the `ListStack`
	 *
	 * @param val The value to push
	 *
	 * @returns Whether the value was pushed successfully
	 */
	public push(val: T): boolean {
		//push the value
		if(this.isEmpty()) { //if the stack is empty
			//then create the new top node
			this._head = new ListNode<T>(val, null);
		} else { //if the stack isn't empty
			//then add a new top node
			this._head = new ListNode<T>(val, this._head);
		}

		//increment the size
		this._size++;

		//and return a success
		return true;
	}

	/**
	 * Pops a value off the `ListStack`
	 *
	 * @returns The value on top of the stack, or `null` if empty
	 */
	public pop(): T | null {
		//pop the top value
		if(this.isEmpty()) { //if the stack is empty
			//then return null
			return null;
		} else { //if the stack is not empty
			//then get the top value
			let ret = this.peek(); 

			//pop the top value
			this._head = this._head!.next;

			//decrease the size
			this._size--;

			//and return what was the top value
			return ret;
		} 
	}

	/**
	 * Clears the stack of all elements
	 */
	public clear(): void {
		//loop and clear the stack
		while(!this.isEmpty()) {
			this.pop();
		}
	}

	/**
	 * Returns the value on top of the `ListStack`
	 *
	 * @returns The value on top of the stack, or `null` if empty
	 */
	public peek(): T | null {
		//return the top value
		if(this.isEmpty()) {
			return null;
		} else {
			return this._head!.value;
		}
	}

	/**
	 * Returns whether the `ListStack` is empty
	 *
	 * @returns Whether the stack is empty
	 */
	public isEmpty(): boolean {
		return this._head === null;
	}
}

/**
 * A node in a linked list
 */
class ListNode<T> {
	//fields
	/**
	 * The value of the `ListNode`
	 */
	private _value: T;

	/**
	 * The next node in the list
	 */
	private _next: ListNode<T> | null;

	//methods
	
	/**
	 * Constructs a new `ListNode` instance
	 *
	 * @param value The value of the node
	 * @param next The next node in the list
	 */
	constructor(value: T, next: ListNode<T> | null) {
		this._value = value; //init the value field
		this._next = next; //init the next node field
	}

	/**
	 * Gets the value of the `ListNode`
	 *
	 * @returns The value of the node
	 */
	public get value(): T {
		return this._value; //return the value field
	}

	/**
	 * Gets the next node in the list
	 *
	 * @returns The next node in the list
	 */
	public get next(): ListNode<T> | null {
		return this._next;
	}
}

//end of file
