/*
 * Stack.ts
 * Defines the interface of the Stack ADT
 * Created on 9/15/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * A generic LIFO data structure
 */
export interface Stack<T> {
	/**
	 * The size of the `Stack`
	 */
	size: number;

	/**
	 * Pushes a value onto the `Stack`
	 *
	 * @param val The value to push
	 *
	 * @returns Whether the value was added successfully
	 */
	push(val: T): boolean;

	/**
	 * Pops a value off of the `Stack`
	 *
	 * @returns The popped value, or `null` if empty
	 */
	pop(): T | null;

	/**
	 * Returns the value on top of the `Stack`
	 *
	 * @returns The value on top of the `Stack`, or `null` if empty
	 */
	peek(): T | null;

	/**
	 * Returns whether the `Stack` is empty
	 *
	 * @returns Whether the `Stack` is empty
	 */
	isEmpty(): boolean;

	/**
	 * Clears the `Stack`
	 */
	clear(): void;
}

//end of file
