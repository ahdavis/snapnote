/*
 * using.ts
 * Defines a linked-list implementation of the Stack ADT
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Disposable } from './Disposable';
import { UsingCallback } from './UsingCallback';

/**
 * Creates a scoped lifetime for an object
 *
 * @param obj The object to manage
 * @param func The function that `obj` is valid inside
 */
export function using<T extends Disposable>(obj: T, 
						func: UsingCallback<T>) {
	//manage the object
	try {
		func(obj);
	} finally {
		obj.dispose();
	}
}

//end of file
