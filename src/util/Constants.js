/*
 * Constants.js
 * Defines a class that manages constants
 * Created on 9/15/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//class definition
// @ts-ignore
class Constants {
	//the width of the window
	static get WIN_WIDTH() {
		return 900;
	}

	//the height of the window
	static get WIN_HEIGHT() {
		return 680;
	}
}

//export the class
exports.Constants = Constants;

//end of file
